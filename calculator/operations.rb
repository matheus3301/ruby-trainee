require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      sanitized_blacklist = blacklist.split(" ")
      sanitized_grades = JSON.parse(grades)
      
      selected_grades = sanitized_grades.select do |name,grade|
        not sanitized_blacklist.include?(name)
      end

      sum = 0
      selected_grades.each do |name,grade|
        sum += grade
      end

      return (sum/selected_grades.size).to_f

    end
  
    def no_integers(numbers)
      number_ending = ["00","25","50","75"]

      sanitized_numbers = numbers.split(" ")
      ans = ""

      sanitized_numbers.each do |number|
        if number.length < 2 || number == "00"
          ans += "N "
        elsif number_ending.include?(number[-2..-1])
          ans += "S "
        else
          ans += "N "        
        end
      end
  
      return ans
    end
  
    def filter_films(genres, year)
      genres = genres.split(" ")
      movies = get_films[:movies]

      ans = []
      movies.each do |movie|
        if((movie[:genres] & genres).length == genres.length && movie[:year].to_i >= year)
          ans.push(movie[:title])
        end
      end      

      return ans

    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
