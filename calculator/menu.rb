require_relative 'operations'

module Calculator
  class Menu
    def initialize
      operations = Operations.new

      puts "-------------------------"
      puts "|Bem vindo a calculadora|"
      puts "-------------------------"
      puts "\nSelecione:"
      puts "1 - Media Preconceituosa"
      puts "2 - Calculadora sem numeros"
      puts "3 - Filtro de Filmes"
      puts "4 - Consulta Usuário GitHub"

      puts "0 - Sair"

      op = gets.chomp.to_i
      system "clear || cls"

      case op
        when 0 
          exit
        when 1
          puts "Insira o JSON com as notas"
          grades = gets.chomp
          puts "Insira, separado por espaços, os nomes dos alunos a serem desconsiderados"
          blacklist = gets.chomp

          avg = operations.biased_mean(grades,blacklist)
          system "clear || cls"          
          puts "A média final é : "+avg.to_s
          puts "Pressione enter para continuar"
          gets.chomp
          system "clear || cls"          
        when 2
          puts "Insira os números a serem calculados"
          numbers = gets.chomp

          ans = operations.no_integers(numbers)
          system "clear || cls"          
          puts "Resultado: "
          puts ans
          puts "Pressione enter para continuar"
          gets.chomp
          system "clear || cls"

        when 3
          puts "Insira as categorias a serem filtradas"
          genres = gets.chomp
          puts "Insira o ano para consulta"
          year = gets.chomp.to_i

          movies = operations.filter_films(genres, year)
          system "clear || cls"          
          puts "Resultado: "
          puts movies
          puts "Pressione enter para continuar"
          gets.chomp
          system "clear || cls"
    

      when 4
          puts "Insira um username do GitHub: "
          username = gets.chomp
          

          info = operations.get_profile(username)
          system "clear || cls"          
          puts "Resultado: "
          if(not info)
            puts "Insira um usuário valido"
          else
            puts "Nome:",info[:name]
            puts "\nRepositórios:",info[:public_repos]
            puts "\nSeguidores:",info[:followers]
            puts "\nBiografia:",info[:bio]
            puts "\nBlog:",info[:blog]
            puts "\nLocalização:",info[:location]




          end
          puts "Pressione enter para continuar"
          gets.chomp
          system "clear || cls"
      
      
      else
        puts "Insira uma operação válida"
      end
    end
  end
end
