require 'net/http'
require 'json'

module ExtraOperations

    def get_profile(username)
      url = 'https://api.github.com/users/'+username
      uri = URI(url)
      response = Net::HTTP.get(uri)
      response = JSON.parse(response, symbolize_names: true)

      if(response[:message] == "Not Found")
          return false
      end

      return response
        
    end
end
